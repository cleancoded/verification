<?php
/**
 * The main plugin file.
 *
 * This file loads the main plugin class and gets things running.
 *
 * @since 1.0
 *
 * @package Cannais_Age_Verification
 */

/**
 * Plugin Name: Cannabis Age Verification
 * Description: Age verification for Cannabis and other age restricted websites.
 * Author:      5 Star Plugins
 * Author URI:  https://5starplugins.com/
 * Version:     1.0
 * Text Domain: cannabis-age-verification
 * Domain Path: /languages
 * 
 * Copyright 2018 5 Star Plugins
 *
 * The following code is a derivative work of the code from Age Verify v0.3.0 by Chase Wiseman, 
 * which is licensed GPLv2. This code therefore is also licensed under the terms 
 * of the GNU Public License, verison 2.                                                     
 *,,,,,,,,,,,,,, fsdfsdfsd
 */

// Don't allow this file to be accessed directly.
if ( ! defined( 'WPINC' ) ) {
	die();
}

/**
 * The main class definition.
 */
require( plugin_dir_path( __FILE__ ) . 'includes/class-age-verify.php' );

// Get the plugin running.
add_action( 'plugins_loaded', array( 'Age_Verify', 'get_instance' ) );

// Check that the admin is loaded.
if ( is_admin() ) {

	/**
	 * The admin class definition.
	 */
	require( plugin_dir_path( __FILE__ ) . 'includes/admin/class-age-verify-admin.php' );

	// Get the plugin's admin running.
	add_action( 'plugins_loaded', array( 'Age_Verify_Admin', 'get_instance' ) );
}
