=== Age Verify ===
Contributors: 5StarPlugins
Tags: age, restrict, verify, cannabis
Requires at least: 3.9
Tested up to: 4.9.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Rewrite... Ask visitors for their age before viewing your content.

== Description ==

Cannabis Age Verification

== Installation ==

1. Upload the 'age-verify' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Visit 'Settings > Age Verify' and adjust your configuration.

== Screenshots ==

1. The settings screen.
2. This is what your visitors see, using the default styling.

== Changelog ==

= 1.0 =
* New version
